---
lastmod: 2021-12-28
title: Cross-chain atomic swap
---

Hash time lock contract or HTLC is a composition of a hash lock and a time lock.
The hash lock authorizes the ClaimHTLC transaction.
The time lock authorizes the RefundHTLC transaction.
The result of this contract is either success or refund, assuming that transactions are timely included to the blockchain.
It is also named an atomic swap.

## Hash lock

Hash lock is a cross-chain authorization mechanism that requires a preimage of a cryptographic hash.

- BLAKE2b-256
- SHA-256
- Keccak-256
- RIPEMD-160

## Time lock

Time lock is a cross-chain authorization mechanism that requires a specified block time or height to pass.

- Absolute block time
- Absolute block height
- Relative block time
- Relative block height

## RPC API

In version 0.3 RPC API will be implemented.

- Create atomic swap |
--- | ---------
Type | POST
Path | /createswap
Parameters |
mnemonic | mnemonic of wallet
fee | transaction fee
amount | amount to transfer
to | address of receiving account
timelocktype | 0 for absolute time, 1 for absolute height, 2 for relative time, 3 for relative height
timelockdata | time or height depending on type
hashlocktype | 0 for BLAKE2b-256, 1 for SHA-256, 2 for Keccak-256, 3 for RIPEMD-160
hashlockdata | HEX-encoded hash
Returns transaction hash.

- Claim atomic swap |
--- | ---------
Type | POST
Path | /claimswap
Parameters |
mnemonic | mnemonic of wallet
fee | transaction fee
id | address of atomic swap
preimage | HEX-encoded preimage
Returns transaction hash.

- Refund atomic swap |
--- | ----------
Type | POST
Path | /refundswap
Parameters |
mnemonic | mnemonic of wallet
fee | transaction fee
id | address of atomic swap
Returns transaction hash.
