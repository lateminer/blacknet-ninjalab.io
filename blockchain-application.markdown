---
lastmod: 2021-12-28
title: Blockchain application
---

Note: API for development of Blacknet blockchain applications (BApp) is ongoing progress.

Blacknet is designed around an idea that applications can use public blockchain technology.
In contrast to many platforms that put decentralized finance at the core, Blacknet compromises composability to achieve scalability for computation heavy applications.
Applications can be Desktop, Mobile, Web, but not limited to these platforms.

Notable use cases:

- Cryptocurrency (crypto) is a tradable digital asset or digital form of money, built on blockchain technology that only exists online.
- Non-fungible token (NFT) is a unique and non-interchangeable unit of data stored on a blockchain.
- Decentralized autonomous organization (DAO) is an organization represented by rules encoded as a computer program that is transparent, controlled by the organization members and not influenced by a central government.
- Decentralized finance (DeFi) is a blockchain-based form of finance that does not rely on central financial intermediaries such as brokerages, exchanges, or banks to offer traditional financial instruments, and instead utilizes smart contracts on a blockchain.
- Self-sovereign identity (SSI) is an approach to digital identity that gives individuals control of their digital identities.
