---
lastmod: 2020-11-11
title: Auditing BLN supply
---

The [genesis](2018-09-06-ibo-announcement.html) state of supply may be advanced in two directions: increased to incentivize participation in proof-of-stake consensus, decreased to produce a proof-of-burn.
With Blacknet proof of stake version 4, the supply has no limit but steady mint rate of about 1%.

Node exposes required information via HTTP API in JSON format.

```
/api/v2/ledger

{
    "height": 1642726,
    "blockHash": "A61C0B098EF068603BEF636F5BFBA303F9A6B2119F68BD1F7A53FC5F5CC020BE",
    "blockTime": 1597673264,
    "rollingCheckpoint": "F8AD69FADB34A679805057ECCF5AC2BEEE3187C429E1A9B74C0F4408F88FF579",
    "difficulty": "434479512564446105478695934332714275672713818281074750291030",
    "cumulativeDifficulty": "322315452613660940068972",
    "supply": "101584232788659698",
    "maxBlockSize": 100000,
    "nxtrng": "4630AFC57DF736A61CDAE6C9FC4E2DE60B693811405DBFDCBACA5EE55FC24FD1"
}
```

With block `A61C0B098EF068603BEF636F5BFBA303F9A6B2119F68BD1F7A53FC5F5CC020BE`, ledger at height `1642726`, present supply is `1015842327.88659698 BLN`.

```
/api/v2/ledger/check

{
    "result": true,
    "accounts": 2060,
    "htlcs": 0,
    "multisigs": 0
}
```

Recalculation of supply involved `2060` accounts and `0` built-in contracts. The result matches expected value.

Note: The lease contract amount counted towards account balance.
