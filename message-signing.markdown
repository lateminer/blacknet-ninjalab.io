---
lastmod: 2020-10-09
title: Text message signing
titleOnlyInHead: 1
---

## Message signing

Blacknet account can sign a message for the purpose of off-chain communication.
Off-chain communication means that the signed message is not submitted to the block chain.

The used digital signature scheme is Ed25519-BLAKE2b.
The message is fed into BLAKE2b-256 hashing function with padding `Blacknet Signed Message:\n`.
Verification requires an accound address, a message signature, an original message.

## RPC

- Sign message |
--- | ---------
Type | POST
Path | /api/v2/signmessage
Parameters |
mnemonic | mnemonic of signing account
message | text message
Returns signature.

- Verify message |
--- | ---------
Type | GET
Path | /api/v2/verifymessage/{from}/{signature}/{message}
Parameters |
from | address of signing account
signature | signature to verify
message | text message
Returns boolean.

## Library

- Dart [https://pub.dev/packages/blacknet_lib](https://pub.dev/packages/blacknet_lib)
- Go [https://gitlab.com/blacknet-ninja/blacknetgo-lib](https://gitlab.com/blacknet-ninja/blacknetgo-lib)
- JavaScript [https://www.npmjs.com/package/blacknetjs](https://www.npmjs.com/package/blacknetjs)
- PHP [https://packagist.org/packages/blacknet/lib](https://packagist.org/packages/blacknet/lib)

## RPC-CLI

- Python [https://github.com/hclivess/blacknet_tools](https://github.com/hclivess/blacknet_tools)
