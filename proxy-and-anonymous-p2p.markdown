---
lastmod: 2020-09-16
title: Proxy and anonymous P2P
---

By default Blacknet node establishes clearnet connections to other nodes and can automatically use locally available supported overlay networks.
Network configuration is stored in file `blacknet.conf`.

## I2P

I2P implements garlic routing.

SAM V3 is supported.
It may be disabled by default in I2P router.
You can enable it in I2P Router Console -> I2P Client Configuration -> SAM application bridge.

I2P bridge for .b32.i2p connections:

```
i2psamhost=127.0.0.1
i2psamport=7656
```

## Tor

Tor implements onion routing.

Onion service v2 is supported.
Tor control port may be disabled by default in `torrc` config file.

Tor proxy for outgoing .onion connections:

```
torhost=127.0.0.1
torport=9050
```

Tor proxy for outgoing TCP/IP connections:

```
listen=false

proxyhost=127.0.0.1
proxyport=9050
```

Tor control port to listen on .onion address:

```
torcontrol=9051
```

## TCP/IP

TCP/IP can be proxified using SOCKS5 protocol:

```
listen=false

proxyhost=127.0.0.1
proxyport=9050
```

or disabled:

```
ipv4=false
ipv6=false
listen=false
```
