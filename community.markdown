---
lastmod: 2022-10-26
title: Community
titleOnlyInHead: 1
---

Blacknet is decentralized peer-to-peer network that secures blockchain-based finance platform with proof of stake consensus mechanism.
Community is worldwide and identities are pseudonymous.
Proof of stake is used as [Sybil resistance mechanism](blacknet-posv4.pdf) of identities in order to prevent a Sybil attack by economic costs of the staking token.
Block version voting by stakehodlers may be used for protocol updates.
Links to chats, forums, exchanges, explorers, pools, and other.

<h3>Exchanges</h3>

We are looking for listings.


<h3>Explorers</h3>

[<i class="fas fa-cubes"></i> You can setup our explorer from source code.](https://gitlab.com/blacknet-ninja/blacknet-explorer)


<h3>Stake pools</h3>

[<i class="fas fa-cubes"></i> blnpool.io](https://www.blnpool.io/)


<h3>Social</h3>

[<i class="fas fa-comments"></i> Matrix community](https://app.element.io/#/group/+blacknet:matrix.org) +blacknet:matrix.org

[<i class="fab fa-bitcoin"></i> BitcoinTalk](https://bitcointalk.org/index.php?topic=469640.0)

[<i class="fab fa-reddit"></i> Reddit](https://old.reddit.com/r/blacknet) r/blacknet

[<i class="fab fa-qq"></i> QQ group](https://www.qq.com/) 705602427

[<i class="fab fa-twitter"></i> Twitter](https://twitter.com/BlacknetN) @BlacknetN

[<i class="fab fa-gitlab"></i> GitLab group](https://gitlab.com/blacknet-ninja)
