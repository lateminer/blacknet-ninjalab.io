---
lastmod: 2020-11-11
title: Token transfer
---

Blacknet account has a reusable address.
This is in contrast to the UTXO model where addresses often are one-time.

You may need one account for processing customer payments.
Transfers are designed to be distinguished by a payment id.
A payment id essentially is a short string that uniquely identifies your customer or their payment.
It should be a randomly generated string in order to not need an optional encryption.
If you do not need an id unique by a payment then an option to generate new payment address is ought to be provided for customers.

We recommend waiting at least 10 confirmations on the blockchain before processing.
Confirmations are not required among trusted parties.
For API see Wallet Documentation.
