---
lastmod: 2022-08-17
title: Download Full Node Wallet
titleOnlyInHead: 1
---

## Core Wallet

Core Wallet is implemented as a full node that provides a builtin wallet with web UI and RPC API.

* Download and unzip the latest release.
* Change directory to `blacknet/bin`
* On UN*X run `./blacknet`
* On Windows run `.\blacknet.bat`
* Web interface is available at [http://localhost:8283/](http://localhost:8283/)

[<i class="fas fa-file-archive"></i> Download v0.2.12](https://gitlab.com/blacknet-ninja/blacknet/-/jobs/2827092533/artifacts/download)

Release type `Update initial nodes`

Release date `August 08 2022`

MD5 checksum `72F7902AA18EDC9A97AEC8CD9C6FF0E9`

### Prerequisite

<i class="fab fa-java"></i> Java 8 or higher.

- Debian & Ubuntu: `sudo apt-get install default-jre`
- Red Hat & Oracle: `sudo yum install java-11-openjdk`
- SUSE: `sudo zypper install java-11-openjdk`
- Arch: `sudo pacman -S --needed jre-openjdk`
- Gentoo: `sudo emerge -av1 --noreplace virtual/jre`
- FreeBSD: `sudo pkg install openjdk11-jre`
- OpenBSD: `sudo pkg_add jdk`
- macOS & Windows: [OpenJDK](https://jdk.java.net/), [Oracle Java](https://java.com/download/), or other

## Source code

Download the source code for building locally.
For build instructions see the README file.

[<i class="fas fa-file-archive"></i> Download v0.2.12](https://gitlab.com/blacknet-ninja/blacknet/-/archive/v0.2.12/blacknet-v0.2.12.zip)

[<i class="fas fa-tags"></i> Download previous release tags](https://gitlab.com/blacknet-ninja/blacknet/-/tags)
